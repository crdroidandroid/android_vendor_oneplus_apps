PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps/avicii

PRODUCT_COPY_FILES += \
    vendor/oneplus/apps/avicii/proprietary/system/etc/permissions/com.oneplus.camera.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.xml \
    vendor/oneplus/apps/avicii/proprietary/system/etc/permissions/com.oneplus.camera.pictureprocessing.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.pictureprocessing.xml \
    vendor/oneplus/apps/avicii/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    vendor/oneplus/apps/avicii/proprietary/system/etc/sysconfig/hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist.xml \
    vendor/oneplus/apps/avicii/proprietary/system_ext/etc/permissions/privapp-permissions-oem-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oem-system_ext.xml \
    vendor/oneplus/apps/avicii/proprietary/system_ext/etc/sysconfig/hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist.xml \
    vendor/oneplus/apps/avicii/proprietary/system_ext/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so

PRODUCT_PACKAGES += \
    OnePlusCameraService \
    OnePlusCamera \
    OnePlusGallery
